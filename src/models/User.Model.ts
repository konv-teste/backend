import mongoose from "../database/index";

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  cpf: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
    select: false,
  },
  createdAt: {
    type: Date,
  },
  updatedAt: {
    type: Date,
  },
  account_balance: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "AccountBalance",
  },
  status: {
    type: String,
    default: "active",
    enum: ["active", "inactive", "closed"],
    required: true,
  },
});

export const User = mongoose.model("UserModel", UserSchema);
