import { User } from "../models/User.Model";
import * as SECRET from "../config/auth.json";
import { Strategy, ExtractJwt } from "passport-jwt";

const options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: SECRET.secret,
};

export const passportFunction = (passport: any) => {
  passport.use(
    new Strategy(options, async (jwt_payload, done) => {
      await User.findById(jwt_payload.id)
        .then(async (user) => {
          if (user) {
            return done(null, user);
          }
          return done(null, false);
        })
        .catch((error) => {
          return done(error, false);
        });
    })
  );
};
