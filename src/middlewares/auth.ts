import Jwt from "jsonwebtoken";
import { Request, Response, NextFunction } from "express";
import bcrypt from "bcryptjs";
import { User } from "../models/User.Model";
import { AccountBalance } from "../models/AccountBalance.Model";
import * as SECRET from "../config/auth.json";
import passport from "passport";

export const userRegister = async (userDetails: any, res: Response) => {
  try {
    const { name, cpf, password } = userDetails;

    if (!isValidCpf(cpf)) {
      return res.status(400).json({
        message: "Invalid CPF.",
        success: false,
      });
    }

    if (!isValidPassword(password)) {
      return res.status(400).json({
        message: "Invalid password. Password must be at least 6 digits long",
        success: false,
      });
    }

    if (await isExistingUser(cpf)) {
      return res.status(400).json({
        message: "User already exists.",
        success: false,
      });
    }

    const user = new User({
      name,
      cpf,
      password: await bcrypt.hash(password, 12),
      createdAt: new Date(),
      updatedAt: new Date(),
    });
    await user.save();

    const accountBalance = new AccountBalance({
      balance: 0,
      transactions: [],
    });
    await accountBalance.save();

    await user.updateOne({ account_balance: accountBalance._id });

    return res.status(201).json({
      message: "User created successfully.",
      success: true,
    });
  } catch (error: any) {
    res.status(500).json({
      message: "Internal server error",
      // error: error.message,
      success: false,
    });
  }
};

export const userLogin = async (userCreds: any, res: Response) => {
  try {
    const { cpf, password } = userCreds;

    if (!isValidCpf(cpf)) {
      return res.status(400).json({
        message: "Invalid CPF. Please, check your CPF number.",
      });
    }

    const user = await User.findOne({ cpf }).select("+password");

    if (!user) {
      return res.status(404).json({
        message: "User not found. Please, check your CPF number.",
        success: false,
      });
    }

    if (user.status !== "active") {
      return res.status(403).json({
        message: "User is inactive. Please, contact the bank.",
        success: false,
      });
    }

    const isMatch = await bcrypt.compare(password, user.password);

    if (!isMatch) {
      return res.status(404).json({
        message: "Invalid password. Please, check your password.",
        success: false,
      });
    }

    const token = Jwt.sign({ id: user._id }, SECRET.secret, {
      expiresIn: "1h",
    });

    const result = {
      name: user.name,
      cpf: user.cpf,
      token: `Bearer ${token}`,
      expiresIn: 180,
    };

    return res.status(200).json({
      ...result,
      message: "User logged successfully.",
      success: true,
    });
  } catch (error: any) {
    res.status(500).json({
      message: "Internal server error",
      // error: error.message,
      success: false,
    });
  }
};

export const userAuth = passport.authenticate("jwt", { session: false });

export const serialize = async (user: any) => {
  const balanceDb = await AccountBalance.findById(user.account_balance);
  return {
    name: user.name,
    cpf: user.cpf,
    createdAt: user.createdAt,
    account_balance: {
      balance: balanceDb?.balance,
      transactions: balanceDb?.transactions,
    },
  };
};

export const serializeNotSafe = async (creds: any, res: Response) => {
  try {
    const { cpf } = creds;

    if (!isValidCpf(cpf)) {
      return res.status(400).json({
        message: "Invalid CPF. Please, check your CPF number.",
      });
    }

    const user = await User.findOne({ cpf });

    if (!user) {
      return res.status(404).json({
        message: "User not found. Please, check your CPF number.",
        success: false,
      });
    }

    const balanceDb = await AccountBalance.findById(user.account_balance);

    return {
      account_balance: {
        balance: balanceDb?.balance,
        transactions: balanceDb?.transactions,
      },
    };
  } catch (error: any) {
    res.status(500).json({
      message: "Internal server error",
      // error: error.message,
      success: false,
    });
  }
};

const isExistingUser = async (cpf: string) => {
  const user = await User.findOne({ cpf });
  return user ? true : false;
};

const isValidCpf = (cpf: string) => {
  const regex =
    /([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})/;
  return regex.test(cpf);
};

const isValidPassword = (password: string) => {
  const regex = /^[0-9]{6,}$/;
  return regex.test(password);
};
