import { Response, Request, Router } from "express";
import { creditOperation } from "../controllers/creditOperation";
import { withdrawOperation } from "../controllers/withdrawOperation";
import { userAuth } from "../middlewares/auth";

export const TransactionsRouter = Router();

TransactionsRouter.post("/credit", async (req: Request, res: Response) => {
  await creditOperation(req, res);
});

TransactionsRouter.post(
  "/withdraw-notsafe",
  async (req: Request, res: Response) => {
    await withdrawOperation(req, res);
  }
);

TransactionsRouter.post(
  "/withdraw",
  userAuth,
  async (req: Request, res: Response) => {
    await withdrawOperation(req, res);
  }
);
