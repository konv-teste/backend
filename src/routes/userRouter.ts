import { Router, Request, Response } from "express";
import {
  serialize,
  serializeNotSafe,
  userAuth,
  userLogin,
  userRegister,
} from "../middlewares/auth";

export const UserRouter = Router();

UserRouter.post("/register", async (req: Request, res: Response) => {
  await userRegister(req.body, res);
});

UserRouter.post("/login", async (req: Request, res: Response) => {
  await userLogin(req.body, res);
});

UserRouter.get("/profile", userAuth, async (req: Request, res: Response) => {
  return res.json(await serialize(req.user));
});

UserRouter.post("/profile", async (req: Request, res: Response) => {
  return res.json(await serializeNotSafe(req.body, res));
});
