import mongoose from "mongoose";
import { config } from "dotenv";
config();

mongoose.connect(process.env.MONGODB_URI as string, {}, (err) => {
  if (err) {
    console.log(err);
  }
  console.log("Connected to MongoDB");
});

mongoose.Promise = global.Promise;

export default mongoose;
