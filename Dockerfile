FROM node:slim as development
RUN mkdir -p /opt/app
WORKDIR /opt/app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

FROM node:slim as production
ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}
WORKDIR /opt/app
COPY package*.json ./
RUN npm install --only=production
COPY --from=development /opt/app/dist ./dist

CMD ["node", "dist/server.js"]