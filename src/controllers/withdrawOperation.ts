import { Request, Response } from "express";
import { AccountBalance } from "../models/AccountBalance.Model";
import { User } from "../models/User.Model";
import { v4 as uuidv4 } from "uuid";

export const withdrawOperation = async (req: Request, res: Response) => {
  try {
    const { cpf, value } = req.body;
    const user = await User.findOne({ cpf });

    if (!user) {
      return res.status(404).json({
        message: "User not found.",
        success: false,
      });
    }

    const accountBalance = await AccountBalance.findById(user.account_balance);

    if (!accountBalance) {
      return res.status(404).json({
        message: "Account balance not found.",
        success: false,
      });
    }

    if (!Number.isInteger(value)) {
      return res.status(400).json({
        message: "Value must be an integer.",
        success: false,
      });
    }

    const bills = exchangeMoney(value);
    if (bills.success === false) {
      return res.status(400).json({
        message: bills.message,
        success: false,
      });
    }

    if (accountBalance.balance < value) {
      return res.status(400).json({
        message: "Not possible to withdraw this amount.",
        success: false,
      });
    }

    await accountBalance.updateOne({
      $inc: { balance: -value },
      $push: {
        transactions: {
          id: uuidv4(),
          value: -value,
          type: "debit",
          createdAt: new Date(),
        },
      },
    });

    return res.status(201).json({
      message: "Transaction created successfully.",
      success: true,
    });
  } catch (err: any) {
    res.status(500).json({
      message: "Internal server error",
      error: err.message,
      success: false,
    });
  }
};

const exchangeMoney = (amount: number) => {
  const bills: any = {
    hundred: 100,
    fifty: 50,
    twenty: 20,
    ten: 10,
    five: 5,
    two: 2,
  };

  let resultBills: any = {};
  let cashLeftover: any = amount;

  for (let key in bills) {
    while (cashLeftover >= bills[key]) {
      if (resultBills[key]) {
        resultBills[key] += 1;
      } else {
        resultBills[key] = 1;
      }
      cashLeftover = (cashLeftover - bills[key]).toFixed(2);
    }
  }

  if (cashLeftover > 0) {
    return {
      success: false,
      message: "Not possible to exchange this amount.",
    };
  }

  return resultBills;
};
