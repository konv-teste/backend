import mongoose from "../database/index";

interface IAccountBalance {
  balance: number;
  transactions: ITransaction[];
}

interface ITransaction {
  id: string;
  value: number;
  type: string;
  createdAt: Date;
}

const AccountBalanceSchema = new mongoose.Schema({
  balance: {
    type: Number,
    required: true,
  },
  transactions: [
    {
      id: { type: String, required: true },
      type: {
        type: String,
        enum: ["credit", "debit"],
        required: true,
      },
      value: { type: Number, required: true },
      createdAt: { type: Date, required: true },
    },
  ],
});

export const AccountBalance = mongoose.model<IAccountBalance>(
  "AccountBalance",
  AccountBalanceSchema
);
