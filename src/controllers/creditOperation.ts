import { Request, Response } from "express";
import { AccountBalance } from "../models/AccountBalance.Model";
import { User } from "../models/User.Model";
import { v4 as uuidv4 } from "uuid";

export const creditOperation = async (req: Request, res: Response) => {
  try {
    const { cpf, value } = req.body;
    const user = await User.findOne({ cpf });

    if (!user) {
      return res.status(404).json({
        message: "User not found.",
        success: false,
      });
    }

    const accountBalance = await AccountBalance.findById(user.account_balance);

    if (!accountBalance) {
      return res.status(404).json({
        message: "Account balance not found.",
        success: false,
      });
    }

    if (!Number.isInteger(value)) {
      return res.status(400).json({
        message: "Value must be an integer.",
        success: false,
      });
    }

    await accountBalance.updateOne({
      $inc: { balance: value },
      $push: {
        transactions: {
          id: uuidv4(),
          value: value,
          type: "credit",
          createdAt: new Date(),
        },
      },
    });

    return res.status(201).json({
      message: "Transaction created successfully.",
      success: true,
    });
  } catch (err: any) {
    res.status(500).json({
      message: "Internal server error",
      error: err.message,
      success: false,
    });
  }
};

Number.isInteger =
  Number.isInteger ||
  function (value) {
    return (
      typeof value === "number" &&
      isFinite(value) &&
      Math.floor(value) === value
    );
  };
