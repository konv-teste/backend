import express from "express";
import Cors from "cors";
import passport from "passport";

import swaggerUi from "swagger-ui-express";
import swaggerFile from "./swagger.json";

import { passportFunction } from "./middlewares/passport";
import { UserRouter } from "./routes/userRouter";
import { TransactionsRouter } from "./routes/transactionsRouter";

const app = express();
const PORT = process.env.PORT || 5000;

app.use(Cors({ origin: "*", credentials: true }));
app.use(express.json());

app.use("/api/docs/swagger", swaggerUi.serve, swaggerUi.setup(swaggerFile));

app.use(passport.initialize());
passportFunction(passport);

app.use("/api/auth", UserRouter);
app.use("/api/auth/transactions", TransactionsRouter);

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
